De la politique comme ressentiment inné de tout être et de son
application concrète et imminente sur terre et pourquoi pas aussi dans
les limbes de l'hyperespace

J'étais au café, à la terrasse, avec Sophie. Ricou, qui milite avec moi
au PD, a déboulé comme un renard pris en chasse et s'est assis sans
demander s'il dérangeait.

-   Trotspin...
-   Oui ? Hé ben, il a quoi, Trotspin ?
-   Il est joskiste !

La révélation nous fit l'effet d'une bombe, le bruit, les débris et les
bouts de viande en moins. Il n'y a qu'en Palestine que les bombes
explosent aux terrasses des cafés. Pas chez nous, tout de même. On sait
vivre, en France.

-   Enfin, il l'était dans sa jeunesse.
-   Oooh putain de merde, j'aurais pas imaginé ça de lui.

Ca avait foutu la conversation en l'air. Il avait le chic, Ricou, pour
qu'on ne s'intéresse qu'a lui... Je soupçonnais la manœuvre faussement
politique qui se terminerait par un « nan, je déconne, bande de
moules ».

-   Et tu tiens ça d'où ? j'ajoutai.
-   Ta sceptitude me fait chier, pour une fois que j'en rajoute pas, il
    a été joskiste, un point c'est tout, c'est marqué dans la presse, je
    l'ai pas inventé, non mais.
-   Attends, c'est pas n'importe quoi comme info, permet qu'on
    s'interroge.

C'est vrai, quoi. Imaginer notre dirigeant chez les sectos à Josky...
Non, ça ne collait pas.

Le serveur est arrivé, avec les cafés. Il n'avait pas le chocolat
viennois de Sophie :

-   Et s'il vous plaît, vous avez oublié mon...
-   Désolé, aujourd'hui, on ne sert pas les bourgeoises. Le patron est
    en grève et il a décidé ça.
-   Ah... ben, je vais vous quitter, alors.
-   Oui, je ne vois plus que ça à faire.
-   Merci.

Le serveur était très ferme. Sophie s'est levée, souriante, a ramassé
son manteau telle une vraie bourgeoise (qu'elle était, d'ailleurs), a
fait la bise au serveur et est enfin partie.

-   Ouf, fit Ricou
-   Vous saviez pour Trotspin ?, dis-je.
-   C'est même pour ça que mon patron est en grève. Mais moi je m'en
    fiche. J'ai quand même mon salaire, alors, joskiste ou pas...

Je sentais que Ricou allait s'énerver. Ca ne rata pas :

-   Non mais ho c'est quoi ce contre-révolutionnaire à la noix !

 Il s'est levé d'un bond et est devenu rouge pivoine en moins de temps
qu'il n'en faut aux supporters de foot pour gueuler « à mort
l'arbitre ! ». Il avait de la gueule, quand il s'énervait. Le serveur
n'était pas tranquille

-   Je disais ça comme ça, faut pas s'énerver, je pensais pas tomber sur
    une bande d'excités de la politique. Moi ça ne me fait pas bander.
-   Mais tu vas le prendre dans la gueule ton café !

Fallait faire quelque chose, ça pouvait dégénérer et pas forcément à
l'avantage de Ricou. Je n'aimais pas la position « bonne conscience »
de  celui qui s'interpose raisonnablement, alors je lui ai balancé mon
café à la tronche le premier, juste avant Ricou. Le temps qu'il se
nettoie les yeux et le visage, dégoulinants de Grand Arabica, on lui
avait sauté dessus, les autres clients de la terrasse aussi, et la
pagaille fut générale.

Elle perdit son contenu proprement politique quand j'ai lancé :

-   Mort à Trotspin le traître !

Une brave andouille s'est redressée et m'a répondu, en me regardant dans
le blanc des yeux :

-   C'est qui ?

C'est là que commença véritablement mon aventure. Suite à cette
anecdote, je suis rentré chez moi, en passant par chez Denis Connard, un
camarade, lui aussi membre du Parti Démoliste, auquel je devais rendre
un bouquin de théorie structuraliste assez effrayant quant on sait que
ces babouins sont allés jusqu'à mettre cette théorie en pratique.
Délesté du poids de l'infamie, je montais les étages quatre à quatre et
arrivai au quatrième étage. Je m'étalai sur le canapé, crevé, en
rêvassant tendrement à ce qu'il fallait faire pour séduire Sophie,
qu'elle daigne enfin trouver un intérêt à ma présence.

J'étais persuadé qu'elle était pour moi. J'en rêvai. Je dormis.

Le réveil fut des plus étranges.

Je n'ai pas ouvert les yeux directement. J'ai voulu me retourner, mais
je n'ai pas pu, j'étais nu, attaché de partout : pieds et mains fixés
par des anneaux en acier sur une table pas confortable, je me demande
comment j'ai pu dormir là-dessus. Alors j'ai ouvert les yeux. Je vous
demande de me croire sur parole, et si l'envie vous prend, vous
vérifierez après la véracité de mes dires. J'étais dans une salle grise,
faiblement éclairée, bourrée de machines électroniques à faire pâlir
n'importe quel informaticien. Quand j'ai tourné la tête, j'ai pris une
décharge électrique qui eut deux effets : d'une part, je ne pus retenir
ma vessie et me pissai dessus (ce n'était pas si désagréable, il faisait
plutôt frisquet) ; d'autre part, cela alerta mes hôtes de mon réveil et
ils vinrent se placer autour de la table pour m'observer.

-   Qui êtes vous ? articulai-je péniblement.

Je n'avais pas le souvenir d'une biture quelconque mais j'avais tous les
symptômes de la gueule de bois.

-    Nous sommes des martiens.

Ca avait le mérite d'être clair. Ils allaient faire des expériences
bizarres sur moi.

-   En quoi puis-je vous être utile ? Je veux bien collaborer, si c'est
    pas douloureux.
-   Merci. Signez là.

Ils me détachèrent pour que je puisse signer. Une fois relevé, j'ai eu
le temps de voir que la salle était grande et qu'il y avait un autre
cobaye comme moi un peu plus loin. J'ai pas posé de questions, on fera
connaissance plus tard. J'ai attrapé le contrat que me tendait le
martien avec la tête de chef, du bout de ses petits bras maigrichons.
J'ai signé.

-   Ce n'est qu'une formalité, rassurez-vous, me dit le chef.
-   Je peux me rallonger ? Je suis encore un peu fatigué...
-   Faîtes, je vous en prie. Qu'on amène un coussin à monsieur !

Il parlait un français superbe ; rien ne laissait transparaître qu'il
fut martien. Sauf, peut-être, sa tête de crevette. Les autres
martiens-crevettes de la salle, une petite dizaine, n'avaient pas ouvert
la bouche. Sans doute ne s'exprimaient-ils pas couramment en français,
ce devait être là l'apanage du chef, ce qui le faisait chef aux yeux des
autres. Tout est histoire de suprématie. Chouette, me dis-je, j'allai
rencontrer une société martienne, de surcroît sans doute de type
féodale, à en croire l'apparente hiérarchie organisationnelle. Enfin,
j'allais pouvoir épater Sophie quand je lui ferais mon rapport...

Je me rendormis heureux. Ces premiers contacts étaient de bon augure.

Le second réveil fut plus doux, dans la mesure ou je savais où j'étais.
Aussi, je n'eus pas peur quand je vis que je n'étais pas dans la même
salle, pas attaché et rhabillé avec une étrange combinaison rose qui
colle à la peau. Ce n'était pas très sexy. La salle était rose, aussi,
remplie de coussins et autres édredons colorés. Le plus curieux, c'était
le plafond : les murs ne le soutenaient pas, il s'éparpillaient et
disparaissaient avant de l'atteindre. Le plafond flottait. Bel exemple
d'architecture martienne. J'étais ravi qu'il m'aient montré tout ça, à
moi.

Un martien-crevette me regardait tendrement. Il avait un carnet à la
main, il devait prendre des notes sur mon anatomie. Mi scientifique, mi
geôlier, pensai-je. Il alerta les autres de mon réveil.

La même dizaine que tout à l'heure arriva ; enfin, il m'est impossible
de dire c'est la même, parce qu'ils se ressemblent tous. Il n'y a rien
de plus dur que de distinguer un martien-crevette d'un autre
martien-crevette, à moins d'être soi-même un martien-crevette. Ils
doivent avoir un sens en plus pour se différencier. C'est souvent comme
ça chez les martiens.

Le chef arriva :

-   Vous voulez un carambar ?
-   Non merci, j'ai déjà des caries.
-   Ben ça alors, on essaie d'être aimable et accueillant, et on se fait
    envoyer paître. On ne se voit pas tous les jours, faudrait y mettre
    un peu du votre !
-   Excusez-moi, je vais le prendre votre carambar, vous vexez pas.

Je l'ai pris et je l'ai avalé tout rond. C'est pas que j'ai l'habitude
de les manger comme ça, mais eux venait de faire de la sorte ; ça tenait
du rituel, alors j'ai fait pareil.

-   Vous pouvez me donner le programme de mes activités, maintenant ?
    j'ai dit.
-   Il faut d'abord attendre que l'autre se réveille aussi. On ne va pas
    vous donner deux fois la même explication, c'est pas rentable. On
    groupe.
-   Vous avez bien raison.
-   Une chose m'obsède : sur terre, vous appréciez vraiment Lara
    Fabian ?

Je n'eut pas le temps de répondre : un cri rauque, énorme, guttural,
long, sombre et grave leur fit tourner la tête. Ils sortirent de la
pièce, et je restai seul avec mon geôlier. Je restai intrigué par ce
cri : qu'est-ce que cela pouvait bien être ? Il me semblait l'avoir déjà
entendu. Ce ne pouvait être un martien-crevette, personne ne les a
jamais entendu crier. Un martien-crevette, ça ne crie pas. Alors... cela
devait être l'autre. Au réveil. Mais je connaissais ce cri. C'était...
Non. Je me refusai à l'admettre. Impulsif et stupide comme il est, ils
ne l'auraient pas fait venir ici, pas lui...

Je décidai d'en avoir le cœur net... Je me levai et me dirigeai vers
l'unique porte de la pièce, celle par où tous étaient sortis. Le
martien-crevette qui me surveillait me suivit, l'air affolé. Sans doute
aurait-il voulu l'accord du chef pour que je me déplace. Mais je n'avais
pas de temps à perdre en diplomatie. Arrivé à la porte, je déchantai.

NON ! Comment est-ce possible ? Mon geôlier tentait tant bien que mal de
me barrer la route, mais c'était trop tard, j'avais vu. Interloqué,
éberlué, je ne bougeais plus. Le martien-crevette s'agitait devant moi
pour que je retourne dans la pièce rose, ce que je finis par faire, car
même en ayant pris conscience de l'horreur de mes hôtes, je ne tenais
pas à me montrer ingrat. Ca se bousculait dans ma tête. La pièce a côté
était somme toute ordinaire, une banale salle de réunion pour
martiens-crevettes. Mais ce qui faisait peur, c'était le poster de
Staline, en grand, sur le mur du fond, au dessus de la tribune des
orateurs. Oui, Staline.

J'attendis le retour de la dizaine.

Ils arrivèrent rapidement.

-   Vous êtes allé trop loin, me dit le chef.
-   Si peu, jusqu'à la porte.
-   Ah bon ? Ce n'est pas ce qu'on m'a dit.
-   Je me disais bien que vous aviez la même moustache que Staline.
-   Oh, merci.

Il baissa les yeux et rougit un peu. De la gène, car il était mis à nu.
C'était touchant.

-   Oui, reprit-il, une bien longue histoire. Longtemps, je l'ai portée,
    et un jour, boum, elle est partie. C'était dur.

Il déglutit, et reprit son histoire.

-   Et pour finir, j'ai du crier Staline pour qu'elle revienne.
-   Comme je vous comprend.

J'étais rassuré. Je repensai au poster ; un militant du PD ne peut
dignement s'accommoder d'une telle imagerie, grossière et insultante,
sauf si elle résulte, et c'était le cas, du désarroi de l'individu face
à son destin. On touchait là à l'essence même de l'existence. Il était
beau, dans sa sensibilité, tout martien-crevette qu'il fut. Je lâchai
une petite larme, ma contribution. Il y avait de la moquette par terre,
si bien qu'elle ne se fit pas mal en tombant. Elle ne me remercia pas
pour autant.

-   L'autre est réveillé, nous allons commencer. Si vous voulez bien
    vous donner la peine de nous suivre... Par ici, par ici.
-   C'est avec une joie non feinte que je m'apprête à suivre vos
    instructions.

Je me mis face à eux et je levai le poing. J'hurlai :

-   Solidarité ! Nous sommes tous des martiens-crevettes !
-   Non mais ça va, on n'est pas sourds, pas la peine de brailler.

Ils m'escortèrent jusqu\'à la salle de réunion. Entre temps, elle
s'était remplie, bourrée de dizaines de martiens-crevettes. Ils étaient
rapide. Cette salle, tout de même, avait des allures de tribunal. Je
n'étais plus tranquille. On me plaça dans un box, sur le coté de la
pièce. Le chef alla s'asseoir en face, sur un piédestal.

C'est à ce moment là qu'ils amenèrent l'autre : je ne m'étais pas
trompé.

-   Ricou ! dis-je, en bondissant dans ses bras.
-   Ah, c'est toi... On m'avait dit qu'il y en avait un autre, mais de
    là à penser que c'était toi...
-   Tu sens la sueur, tu aurais pu te doucher, c'est désobligeant pour
    nos hôtes.
-   Nan, je me suis pissé dessus au réveil, tu sais, la décharge...
-   Ca alors, il m'est arrivé la même chose ! Alors toi aussi, ils t'ont
    pris ?
-   SILENCE ! hurla le chef.

Et le silence se fit. C'était beau, cette transmission instantanée.

-   Ceci est un procès, reprit-il.

Je m'en serai douté.

-   Vous deux ! dit-il en s'adressant à nous. Ou étiez-vous le 28
    février 2001 à midi ?
-   M'rappelle plus, je dit.
-   Moi non plus, fit Ricou.

Il suait à grosses gouttes. Il n'était pas à l'aise. De toute évidence,
il cachait quelque chose.

C'était lui ou moi. Je me suis décidé. Fallait que je le balance. Tant
pis pour l'amitié.

-   C'est lui, je le reconnais ! fis-je, en désignant Ricou du doigt.
-    On en tient un, fit le chef. Je vais vous dire où vous étiez le 28
    février 2001 à midi pétante : vous vous cachiez dans les Cacatombes
    pour...

Le souvenir de ce funeste repas me revint à la mémoire. J'avais voulu me
débarrasser de tout ce que j'avais fait ce jour-là, j'avais tout
refoulé. Mais c'était ressorti. Et j'étais perdu.

-   ...manger des chips à la crevette.
-   Ooooooooooh, fit la salle, horrifiée.

Oui, je m'étais abaissé à faire ça. J'avais honte.

-   Un simple moment d'oubli que l'on me pardonnera aisément, compte
    tenu du fait que mes parents sont divorcés, dis-je, en me levant,
    pour prouver ma sincérité.
-   Aaaaaaaaaah, fit la salle, dégouttée.
-   C'est moche, fit le chef, vous êtes innocenté. Quant à vous, dit-il
    en se tournant vers Ricou, puisque vous n'avez rien dit, c'est que
    vous êtes coupable.

 Ah, la terrible sensation de voir partir quelqu'un qui vous est
proche ! Ricou, je voudrais te dire combien je t'ai toujours aimé, mon
cœur se déchire... Je suis avec toi, quelque soit la sentence !

-   Alors, fit le chef, puisque vous êtes deux et qu'on ne veux pas
    faire de jaloux, vous partagerez la même peine.
-   Avec joie, dis-je.
-   Bof, dit Ricou.
-   Vous êtes condamnés à consacrer votre carrière militante, au sein du
    PD, au retour de Staline sur terre.
-   Pffffoooouuu, fit la salle, impressionnée.

Ah ! Infamie rampante, trahison impardonnable, que vais-je devenir ?
Tout ça pour quelques minutes d'égarements, une erreur de jeunesse...
Oui, des chips à la crevette... Le retour de Staline... Ô, Sophie, si tu
me voyais, tu ne serais pas fière... Le sol s'ouvre sous mes pieds, et
je tombe, tel une chiffe molle... Je suis foutu...

-   Remarquez, vous auriez pu nous demander de voter Josky aux
    prochaines législatives, dit Ricou.
-   On n'a pas osé vous condamner à ça, c'était quand même trop
    horrible, dit le chef..
-   Merci, on a dit, Ricou et moi, en même temps.

Deux dizaines s'approchèrent de nous pour nous reconduire dans nos
pièces roses respectives. J'eu juste le temps de susurrer un mot à
l'oreille de Ricou avant qu'ils nous séparent :

-   Staline, jamais. Le PD n'est pas un centre d'expérimentation
    sociale. Ou alors, il faudrait repenser la dialectique hédoniste
    actuelle... sans compter le remembrement organisationnel que
    généreraient de telles initiatives. N'oublions pas que l'état actuel
    du PD est la résultante d'une combinatoire de donnés,
    essentiellement issues de la tradition existentialiste de nos
    militants. Certes, il faut conscientiser les classes, puiser nos
    forces productives dans l'antimonde agissant, mais Staline... tout
    dictateur qu'il est, il risquerait de rétablir les cartes de
    rationnement des minutes de télécommunications sur nos téléphones
    portables, et ça... non. Je reste un démoliste convaincu. Je ne
    trahirai pas Trotspin, moi. Imaginer le PD stalinien... non. Ce
    soir, on s'échappe.
-   Ouais, ajouta-t-il, convaincu.

Raccompagné par ma dizaine, je repensai au chef. Ah, l'enfant de putain,
s'il savait ce qui se tramait contre lui !

Arrivé dans la pièce rose, je m'assis sur l'édredon qui m'avait vu me
réveiller.

Plus tard, le chef vint s'entretenir personnellement avec moi. Il fit
signe à la dizaine pour qu'elle sorte, la discussion devait revêtir un
caractère particulièrement solennel pour que soient prises de telles
dispositions. Enfin, on allait parler d'homme à homme, du fond des
choses, loin de toute fioriture sociale. Le vrai débat. Tout mettre à
plat. Tout déballer, et discuter. Un moment vrai. J'en frémissais.

Le chef avait sous le bras une chemise de documents sans doute
importants car elle était cadenassée.

-   Alors, vous refusez.
-   Et comment, que je refuse.
-   J'ai les moyens de vous faire plier. Dans cette pochette. Le sort
    qu'on vous réserve si vous persistez à refuser.

Il l'ouvrit et me fit passer des photos. Il commentait :

-   Alors là, c'est mon cousin Albert qui était au ski avec sa doudoune
    achetée en solde.

Les photos suivantes étaient plus gore : elles montraient des
martiens-crevettes torturés par d'autres martiens-crevettes, du sang,
des armes et des tripes. Beurk, quoi.

Une photo retint particulièrement mon attention. Je la regardai avec
attention, puis m'exclamait :

-   Ciel ! Des martiens-crevettes sodomites !

Le chef était tout embarrassé, car cette photo le montrait, lui, dans
une position sans équivoque avec un jeune martien-crevette pas encore
majeur. De quoi l'inculper grave.

-   Ah, dit-il, vous savez, ce que morale réprouve, souvent bonheur
    l'exige...
-   Je ne suis pas là pour vous juger.
-   Ne dites rien de ce que vous avez vu, et je vous fait une faveur.
-   NON ! Je vais vous faire chanter ! Mon silence contre ma liberté !
-   Très bien. Assommez-moi tout de suite et barrez-vous.

Ce que je fis. Peut-être un peu trop fort : je lui éclatai la tronche.
Au moins, il ne se réveillerait pas de sitôt. J'eus quelques vagues
remords, non pas à cause de la sympathie naissante que j'éprouvais à son
égard, mais parce que je ne lui avais pas demandé où était la sortie et
que je n'étais pas sûr de trouver un autre martien-crevette qui puisse
me l'expliquer en français.

Néanmoins, devant l'état d'avancement de la situation, il me fallait
agir. Nul doute que, si je restai là quelques minutes de plus, la
dizaine de martiens-crevettes qui attendait devant la porte entrerait
et, me voyant ainsi avec les débris du chef, me réserverait un supplice
mortel. Je décidai de partir. De toute façon, je n'avais plus rien à
faire là, ayant refusé leur offre. Je me levai et me dirigeai vers la
porte.

Et, fatalement, la dizaine entra à ce moment là.

-   Ce n'est pas ce que vous croyez, dis-je, il s'est brusquement
    suicidé, j'ai rien pu faire.
-   Ooaff, vous savez, nous, ce qu'on en pense... fit l'un d'eux,
    désinvolte.
-   Ne vous en faîtes pas, on a une cellule de soutien psychologique, si
    vous voulez, dit un autre.
-   Nous sommes un groupe d'opposition armé clandestin, et nous parlons
    français, ajouta un troisième.
-   Apparemment, vous cherchiez la sortie, on peut vous aider, continua
    un quatrième.
-   Nous n'aimons pas la politique du chef et nous favorisons toute
    tentative de subversion et d'opposition, persista le troisième.
-   Voici une carte et demmerdez-vous un peu, on n'est pas des nounous,
    s'énerva le quatrième.
-   Merci, dis-je. Vous n'auriez pas une cigarette par hasard ?
-   Non. Ouste. Partez. Fissa, dit le quatrième, exaspéré.
-   Des cigarettes... Va donc, hé, nazi ! ajouta le troisième.

Je partis.

Arrivé à la porte, je pensai à Ricou : ne valait-il mieux pas le laisser
ici ? Il serait plus utile qu'au PD, en tout cas.

Je regardais la carte : à droite, des escaliers ; à gauche, des
couloirs. Je ne savais comment optimiser ma fuite, alors j'optai pour
les couloirs, moins fatiguant, pensai-je.

Il était désormais manifeste que Ricou allait rester là, en tout cas je
préférais sauver ma peau plutôt que les deux nôtre. Moins risqué, plus
rapide. Ce fut un plaisir d'avoir milité avec toi, Ricou.

Plus je pensai à ce qui ne me liait plus à Ricou, moins je pensais à
trouver la sortie. Evidemment, chez les martiens-crevettes, on ne trouve
pas ces panonceaux lumineux verts qui indiquent la sortie la plus
proche. Ils ne sont pas aux normes, c'est chiant. Va falloir qu'ils s'y
mettent si un jour on veut construire une entente internationale qui ait
de la gueule.

Fatalement, je me suis perdu : je me suis retrouvé devant la pièce rose
où l'on m'avait enfermé ; je craignais d'y trouver des
martiens-crevettes échauffés par ma fuite.

Ca ne rata pas.

Une dizaine commençait à sortir de la pièce : je m'aplatis contre le mur
afin qu'ils ne me vissent point. Ma manœuvre foira, j'étais trop
mal-à-l'aise dans ma combinaison rose pour réagir prestement. Je chutai.
J'étais cuit. J'allais mourir. Ma vie défila dans ma tête, trop vite
pour que je puisse en retenir quoi que ce soit. Un mauvais film, sans
happy end.

De leur coté, les martiens-crevettes de la dizaine se montrèrent moins
romantiques et plus prompts à entamer des négociations un chouïa
belliqueuse : l'un dégaina son gunblaster qui pendait jusque là
négligemment à sa ceinture et me tira dessus :

-   Tacatacatacatacat ! fit le gunblaster

Ce à quoi je répondis :

-   C'est une violation du traité de Megève ! Vous n'avez pas le droit
    de tirer sur un homme désarmé !

Ils se concertèrent cinq minutes, puis décidèrent de me donner ma
chance. Ils m'envoyèrent un gunblaster. Ils venaient de commettre une
belle et énorme bourde.

Je me levai. Rien que par ce geste, j'affirmais ma supériorité : ils
avaient beau être dix, j'étais quand même le plus grand. Je sentis
l'effroi les glacer. Ils eurent un léger mouvement de recul.

J'en profitai.

-   Tacatacatacatacatacatacatacatacatacatacatacatacatacatacatac ! fit
    mon gunblaster.

Et pof ils étaient tous morts. Je me penchai sur leurs cadavres afin de
récupérer leurs armes. Je me chargeai en munitions de gunblaster et en
grenades Atom. Je pris même une chainsaw, au cas où. Et puis, en
bandoulière, elle n'était pas des plus gênantes.

Ce coup-ci, il me fallait les quitter pour de bon. Je regardais la
carte, sans grande conviction, car au fond je savais pertinemment, j'en
avais eu un triste exemple encore tout à l'heure, que mon sens de
l'orientation n'avait d'autre équivalent que celui d'un grille pain
tchèque. Il ne restai qu'une solution viable, et, bien qu'il m'en
coûtât, j'hurlai :

-   Ricoooooooooooooouuuuuuuuuuuuuuuuuuuu ! lequel ne tarda pas à se
    manifester.
-   Ouais, c'est bon, j'arrive.
-   Non, c'est pas bon, j'ai perdu la sortie. Tiens, arme-toi, on se
    tire.
-   Ca avait l'air sympa, votre petite sauterie... fit-il en voyant les
    cadavres.
-   Allez, on s'arrache, dis-je en l'entrainant par le bras.

Je nous dirigeais vers les escaliers. Je ne savais pas si nous nous
trouvions sur une planète ou bien si nous étions dans un vaisseau, mais,
en toute logique, il y avait forcément une sortie extérieure vers le
haut. Ce pourquoi nous prîmes les escaliers ascendants.

Mais une autre dizaine nous avait pris en chasse, et les gunblasters
reprirent leur tragique conversation.

-   Tacatacatac ! fit un des leur.

Je me dis qu'il valait mieux les rassasier d'une grenade Atom ; c'était
sûrement plus ravageur dans une cage d'escaliers.

-   Kabrraoum ! fit la grenade.
-   On les termine à la chainsaw, je lançai à Ricou.
-   Vrrrrrrrrrrrrrrrrrrr, fit la chainsaw, visiblement heureuse de
    servir à nouveau.

On est descendus d'un palier et on a découpé ceux qui n'étaient pas
totalement éclatés par la grenade. Y'en avait partout. Je vomis. Puis
nous reprîmes notre ascension.

Pas très longtemps : des rafales de gunblaster se firent entendre un
étage ou deux au-dessus. Je pris Ricou par le bras et nous nous enfuîmes
par les couloirs sur le palier. Je sentais ma dernière heure arriver,
mais j'étais bourré d'adrénaline, si je devais mourir ce serait en
niquant le maximum de ces cloportes qui n'en veulent qu'à ma liberté.

On s'est arrêté au milieu du couloir :

-   Stop. On se met là, et on les explose dès qu'ils montrent leur
    tronches.
-   OK, fit Ricou.
-   Allez, toi, un genoux à terre, et moi debout derrière, c'est comme
    ça que c'est le plus efficace, j'ai vu un téléfilm là dessus.

On s'est mis en position. Debout, le gunblaster tendu devant moi,
légèrement de profil, j'avais la classe du tueur. On allait niquer leur
putain de mère. Il y aurait du sang partout et ça serait violent.
Déchaîner toutes ces vieilles rancœurs et tirer, tirer...

Il faisait chaud, on avait couru, et j'avais des gouttes de sueur qui me
coulaient le long de la joue. J'en avais mal aux jambes à force de
rester debout.

Je ne sais pas combien de temps on est restés comme ça. Dix minutes,
deux heures, une demi-journée. Les martiens-crevettes n'étaient pas
venus jusqu'à nous. Il ne pouvait y avoir que deux raisons : soit ils
avaient peur de nous (faut dire que notre puissance de feu était
remarquable), soit on les avait déjà tous tués. J'en avais marre de les
attendre, et j'étais assez téméraire pour aller les trouver et qu'un en
finisse une bonne fois pour toutes.

-   Allez on bouge, j'ai dit.
-   Enfin, j'ai plus de genoux, et j'ai faim aussi.
-   T'inquiètes, on va bouffer du martien-crevette.
-   Ah fais gaffe, la dernière fois qu'on a mangé des chips à la
    crevette, rappelle-toi où ça nous a menés...

Ricou s'est levé et on est parti. Arrivé à la porte de l'escalier, on a
entendu du bruit, des coups de feu, des râles, des explosions... Il y
avait des combats. Mais contre qui ? Pas nous. La situation m'échappait.
C'était pourtant nous, les fauteurs de trouble ! Hé, ho, là, c'est pas
du jeu, me dites pas qu'on s'est fait chier pour rien...

Ricou a ouvert la porte et on est passé sur le palier, on est descendu
car les bruits venaient de plus bas. Un martien-crevette isolé est passé
en courant devant nous, sans nous voir, sans s'arrêter. Je me suis lancé
à sa poursuite, je l'ai vite rattrapé, ben oui avec leur petites jambes
ils ont un peu de mal, j'attrapé la chainsaw dans mon dos et
vrrrrrrrrrrrrr floooaaap splitch je l'ai découpé oh il m'a pas vu venir
il aura pas souffert. Je soufflais. Quel speed ! L'occase était trop
belle, je l'ai pas loupée. Et je ne l'avais pas loupé non plus,
d'ailleurs. La chainsaw se sentait toute fière.

J'ai retrouvé Ricou, un peu plus bas, et il discutait avec des
martiens-crevettes. Ah, le traître, il sympathisait avec l'ennemi ! En
plus, c'est sûr, me dis-je, il doit vendre ma peau ! Il me donne, comme
ça il sera libre ! Le salaud !

Je repris la chainsaw et m'approchais d'eux, discrètement. La haine
aidant, ma décision était prise. D'abord, découper Ricou, de dos, c'est
plus facile, après je m'attaquerai aux martiens-crevettes.

Mais l'un d'eux me vit arriver :

-   Attention !, fit-il.

A mon grand étonnement, il parlait français. Ricou s'écarta à temps,
pour lui, et la chainsaw, que j'avais fermement propulsé dans sa
direction, alla se planter dans le mur. C'est là que je reconnu le
groupe d'opposition armé clandestin.

-   Une chance qu'on tombe sur vous, fis-je, joyeux et presque ému par
    de telles retrouvailles totalement incongrues.
-   Nous étions en train d'expliquer à votre camarade le pourquoi du
    comment du barouf actuel. Nous avons profité de votre escapade pour
    tenter une prise de pouvoir. Ca bastonne sec, hein, c'est pas gagné.
-   Et la sortie, elle est où ?, j'ai demandé.
-   Ben en fait, y'en a pas vraiment, vous êtes pas arrivés chez nous
    par une porte...
-   Hé, je suis claustrophobe, faut que je sorte, j'ai dit, en mentant
    pertinemment.
-   Vous vous rappelez la pièce où vous vous êtes réveillés, une grande
    salle grise et froide ?
-   Ouaip.
-   C'est là qu'il vous faut aller. On vous rendort et on vous renvoie
    chez vous.
-   C'est sûr, ce coup-ci ?
-   Oui oui oui, et si vous trouvez moins cher ailleurs on vous
    rembourse la différence.
-   Farpait. On y va ?
-   Euh, en fait il faut juste descendre cet escalier sans se faire
    exploser la cheutron, donc on va y aller, mais soit on la joue
    discret et on tente de passer inaperçu, soit on y va bourrin et on
    les nique en premier. A vous de voir.

Les combats continuaient toujours en dessous. De temps en temps, un
projectile nous atteignait en s'excusant presque : après tout, il ne
faisait que son métier.

Je regardais Ricou et l'interrogeais du regard, mais ma décision était
prise : je serais toujours du coté des trouble-fête. Ricou n'avait l'air
de rien, comme d'hab.

-   J'ai pas envie de me cacher, fis-je en réarmant le chargeur de mon
    gunblaster.
-   Par contre, faut nous laisser les combinaisons roses, on en aura
    besoin pour les prochains, si vous pouviez vous déshabiller
    maintenant...
-   ça évitera d'avoir à se presser là-bas, dis-je, compréhensif.

Alors on s'est déshabillé, Ricou et moi. Pas facile à enlever, ces
combinaisons, ça colle à la peau. Et en plus elle sont moches : pas
faché de les laisser là.

-   Bon, on peut y aller, là, c'est bon.
-   On est partis, fit un des martiens-crevettes, en ajustant son
    gunblaster en position de tir. Il prit la tête du peloton.

Cet escadron faisait plaisir à voir, et pas seulement parce que j'en
faisais partie. Certains avaient des allures de guérilleros, harnachés
un peu n'importe comment. D'autres faisaient vraiment penser à des
soldats d'une armée régulière. Les armes étaient diverses. J'étais en
confiance. On allait rentrer chez nous. Happy end, enfin.

Deux étages à descendre, je vous passe les détails du carnage. J'ai
juste eu le léger sentiment qu'il ne fallait pas décrire cette scène à
tout le monde, surtout à Sophie, parce que, bon, écrabouiller du
martien-crevette réactionnaire avec un gunblaster à la main et la bite
au vent, c'est quand même pas terrible comme image de marque, enfin moi
je suis content d'avoir eu cette expérience, quand bien même elle serait
jugée nauséabonde, mais je n'irai pas m'en vanter publiquement, par
souci du « qu'en dira-t-on » qui risque de bousculer mes chances
d'arriver à une situation sociale honorable, surtout s'il faut que je
rentre dans la polémique surannée qui fait de Staline un ennemi du PD,
alors là ce serait le bouquet, non, non, non, moins j'en dis mieux
c'est.

On a fini par arriver, non sans pertes, à la salle grise. Il ne fallait
rien emporter, alors on a tout laissé. Tout d'un coup, j'eu un doute, la
sensation d'avoir été dupé, et j'en fis part aux martiens-crevettes.

En montant sur la table, alors que Ricou en faisait autant, je leur
demandais :

-   Une chose m'échappe... Ne m'en voulez pas si je me montre sceptique
    et si je remet en cause l'Histoire... Mais pourquoi nous avoir donné
    une carte si la seule sortie possible est ici ?
-   Ah... Nous espérions que vous n'en viendriez pas là. D'abord on vous
    attache, ensuite on vous répond, parce que la réponse est moche, et
    nous on a une révolution sur le feu, alors pas question de la
    gâcher.
-   OK, sans problème, je peux attendre.

Ils m'attachèrent consciencieusement. Ca faisait plaisir à voir,
l'ardeur avec laquelle ils se consacraient à leur action politique ; et
même si je n'y comprenais pas grand chose, entre leurs intestines luttes
de pouvoir, j'étais heureux d'y avoir assisté. Ah, si tous les militants
du PD pouvait avoir autant de détermination...

Ca y est, j'étais attaché.

-   Alors ? Pourquoi cette carte qui ne servait à rien ?
-   Bien. Vous n'êtes pas cardiaque, au moins ? Non, je vous demande ça
    parce que ce serait con que vous nous fassiez un infarctus, là,
    comme ça.
-   Nan, je crains rien, moi.
-   Ooooooh, j'hésite, j'hésite, je sais pas si je dois vous le dire,
    c'est terrible, vous savez, bon allez je me lance, mais j'ai le
    trac, hein, c'est pas facile, tenez en compte.

En fait, on s'est servi de vous pour fomenter notre révolution : on se
doutait que vous feriez n'importe quoi, à tirer dans le tas et à
dégommer des réactionnaires à la pelle. Donc on vous a donné des armes
et on vous a laissé filer, ça faisait diversion et ça nous était utile.
J'ai un peu honte de vous avoir trompé, mais...

-   Booaf, vous savez, hein, moi, j'en ai fait tellement, plus rien ne
    m'étonne.
-   Alors juste avant de partir, il faut réciter une formule magique,
    certes stupide et qui ne sert à rien, mais c'est la règle, et puis
    ça rajoutera un peu de mysticisme dans votre aventure, comme ça vous
    pourrez confirmer le côté alchimique de notre civilisation, qui
    fascine tant la vôtre.

Il me montra un vieux grimoire : la formule était en deux partie. La
première pour lui, la seconde pour moi. Il me fit signe de ne pas la
prononcer avant son signal, sous peine de tout faire foirer. J'étais
attaché sur la table, prêt à partir. Il s'approcha de moi, et me récita
la première partie de la formule et j'enchaînais avec la seconde :

-   Est-ce clair, esclave ?
-   C'est dur à admettre, maître.

Je me suis réveillé sur mon canapé, là où je m'étais endormi, à cette
différence près que j'étais nu. Je me souvenais de tout. Je décidais
pourtant de me taire : qui pourrait croire à l'absurde mais réelle
volonté qu'on les martiens-crevettes de restituer Staline sur terre ?

A ce moment là, on a sonné à la porte, et moi j'étais à poil, et
pourtant il fallait que j'aille ouvrir, alors j'y suis allé tel quel, et
puis zut, faut pas s'effaroucher pour si peu, mais j'ai quand même mis
ma main devant mon sexe parce qu'on sait jamais sur qui on peut tomber
et j'avais pas envie de me traîner des procès pour outrage aux bonnes
mœurs ça peut aller chercher loin faut pas déconner avec ça. J'ai
ouvert, ce n'était que Denis Connard.

-   Ben toi, j'ai dit, surpris.
-   Habille-toi, je t'embarque.
-   Ouais, ouais, j'arrive.

J'ai enfilé des vieilles fringues et je l'ai suivi.

-   On va où ? C'est quoi cette histoire ?
-   Qu'est-ce que tu foutais à dormir à une pareille ? Tu traçais des
    perspectives ?
-   Nan, je modélisais des concepts.

Si tu savais ce que c'est dur de se savoir détenteur d'un secret
impliquant le devenir de l'humanité et de ne pouvoir en parler sous
peine de passer pour un brave guignol mythomane...

-   Mais d'abord c'est à toi de me répondre, j'ai dit, l'air de rien.
-   On va au café.

On est arrivé au café, mais on s'est pas assis en terrasse comme
d'habitude, on est allé directement au fond, et même tout au fond, une
arrière-salle crasseuse, et là il y avait des chaises et une table et
une lampe faible et Denis m'a dit assied-toi je vais chercher mon frère.
Ouaouh. Je savais pas qu'il avait un frère et je savais pas qu'il
connaissait cette pièce que moi-même je connaissais pas. Mais je me suis
assis et j'ai attendu parce que j' étais intrigué.

Denis est revenu avec son frère, et j'ai pas tout de suite compris parce
qu'il est arrivé avec le serveur qui avait encore des traces de coups
sur la tête alors je me suis dit oups le serveur c'est son frère il va
me casser la gueule. J'eu un mouvement de recul.

-   Calme, a dit Denis. Voici mon frère, Steven.
-   Enchanté, Steven Connard, a dit Steven en me tendant sa main.

Il s'est assis sans me filer des baffes. Curieux. Je ne voyais pas où il
voulait en venir.

Denis s'est assis. Et là, il a dit :

-   Où en es-tu, avec Staline ?

Ben là ça m'a quand même estomaqué. Je ne savais pas s'il savait, je ne
savais plus quoi dire, alors j'ai rien dit. Il a continué :

-   Nous sommes, mon frère et moi, devant l'obligation de te révéler
    notre véritable nature, suite aux évènements provoqués par Ricou et
    toi et ayant entraîné un bouleversement dans l'ordre politique
    cyberspatial. Nous sommes...
-   ...Des martiens-crevettes, continua Steven, infiltré sur Terre, au
    PD.
-   Non... Fis-je, incrédule.
-   Si si si, fit Denis.
-   Et on va tout faire pour que tu mènes ta mission à bien, parce que
    pour te surveiller, t'inquiètes pas, on n'est pas les seuls
    martiens-crevettes sur Terre.
-   Mais je vais pas me laisser faire, hein, votre Staline, vous pouvez
    vous le carrer où je pense... Et puis qu'est-ce qui me prouve que
    vous en êtes bien ?

Il y eut un silence. Ils se sont regardés. Puis ils se sont pris la tête
entre les mains et se la sont enlevée comme un vulgaire masque, et en
dessous, il y avait bien la minuscule tronche des martiens-crevettes. Je
pâlis.

-   Il ne faut jamais sous-estimer les Connard.

Tout était dit. Il me fallait agir. Le poids du monde reposait sur mes
épaules.

Nuit Blanche
============

Vers vingt heures quinze, je me suis pointé à Alternation, pensant être
légèrement à la bourre pour le concert. Sur place les Argentins
réglaient encore leur son, et, ô surprise, il y avait même une console
avec un ingé derrière. Je suis allé trouver Christophe qui m'a dit que
non il n'y avait rien d'enregistré ce soir et que c'était juste pour le
son qu'il y avait ce matos. Etonnant. Dans la cave, il faisait plutôt
froid, et il n'y avait pas grand monde. Momo m'a dit que le concert ne
débuterait pas avant 22h, avec Papa Ours, eux après, et La Marquise pour
finir. Chié : j'ai laissé tomber les copains de la CNT alors que
j'aurais pu rester avec eux jusqu\'à la fin de la projection. Papa Ours,
je ne connaissais que de nom ; Radikal Satan, je suis un fan de la
1^ère^ heure, genre groupie que même quand ils font un truc nul je
trouve ça génial ; et La Marquise, en fait, je l'avais déjà croisé chez
Charlotte, un jour où Sylvain et Térèze étaient sur Paris. La soirée
commence en attente.

Je discute avec les uns et les autres, Adrian, Mélo, César, salue les
nouveaux arrivants que je connais, ceux de Kliton, les réguliers
d'Alternation, et la salle se remplit petit à petit de son habituel
public de squat, punks divers, junkies, branchés, quelques chiens, des
teufeurs, etc, et des gens comme moi à l'affût de l'indépendance. Bières
et cigarettes envahissent le lieu.

Papa Ours se décide enfin à jouer, et je me retrouve sur le côté. Pas
terrible pour entendre les paroles, ils jouent en acoustique. Yannick
chante des trucs plutôt libertaires, contre les flics, c'est assez
chouette. Il reprend même du Boris Vian, c'était *Je bois*, je crois.
Ils avancent encore et là je suis carrément derrière eux, j'ai les
jambes faiblardes, alors je vais me poser vers Christophe, qui assure la
distribution, enfin, le mot est grand, il a la charge de vendre le CD de
Radikal et puis quelques 33 tours dont celui de Glen or Glenda et un de
Manuel J. Grotesk - les productions de Kliton - et une compilation des
Potagers Nature. Un gars vient déposer un bouquin sur la table, *la
philosophie du punk*, dépôt-vente, il repassera plus tard voir s'il en a
vendu. Les Argentins se préparent, rouge à lèvre et costume de scène.
Ils attaquent quelques minutes après Papa Ours. Il y a du monde pour les
écouter, je ne peux même pas être en première ligne, je me retrouve
appuyé contre la baffle, je ne vois ni César ni Momo, tout juste Chichi,
et mes oreilles en prennent plein la gueule, mais ça vaut le coup. C'est
quelque part entre free-jazz ludique et punk-tango énervé, il y a des
invités sur chaque morceau, aux percus, et puis aux guitares sur le
dernier morceau qui termine en noise électro-acoustique super violent.
Le pied. J'adore. Je vais dire bonjour à Léna, que je n'attendais pas
là, et je vais voir La Marquise, sur le flyer c'était marqué performance
porno-politik, il est à poil dans sa cage et réclame de la musique. Je
m'assois parce que là j'ai vraiment les jambes en compote. Il ne se
passe rien, La Marquise se dandine à peine. Je sors de la cave pour
aller aux chiottes, il y a un peu de queue, un gars me demande si j'ai
du prod, euh non, j'en ai pas, ah et tu viens d'où, ben en fait je suis
toulousain mais ça fait un an que je suis à Paris et puis je vais
pisser. Je redescend à la cave, je regarde ma montre, minuit trent-cinq,
raté pour le dernier métro, mais en fait je m'en fous, comme c'est la
Nuit Blanche y'a des transports toute la nuit.

Les musiciens rangent leur matos dans une salle qui ferme à clef, mais
pour y accéder, faut traverser tout le squat, qui est énorme. Je reste
dans la cave pour garder disques, sacs et blousons, pendant qu'ils
amènent tout là-bas. Adrian arrive chercher les deux derniers amplis et
m'invite à le suivre, mais derrière moi il reste un sac que personne
n'est venu chercher, alors on regarde dedans, y'a l'appareil photo de
Anne, la copine de César, ok, on remonte avec. Momo arrive et prend le
sac, il s'en occupe. Je reste dans la cour, avec Léna, on attend le
reste de la troupe pour bouger. Dans la cour, des gens rébous voire
perchés, des sculptures métalliques et des fauteuils déchirés. Anne
passe devant moi complètement affolée, je la rattrape et lui glisse un
ton sac c'est Momo qui l'a pris, mais comment tu sais que c'est mon sac
que je cherche. Mélo vient nous chercher, tous sont au local, on les
rejoint, on traverse tout le squat, et c'est vraiment immense, je m'y
perdrai, y'a des salles et des ateliers partout, des escaliers à n'en
plus finir, le local est en bas, à côté d'une pièce immense, blanche,
qui devait être un parking souterrain et qui sert apparemment de salle
d'expo. On décide de bouger. On retrouve la sortie, on atterrit dans la
cour, il se met à pleuvoir, on attend encore que tout le monde soit là,
La Marquise passe en long manteau de duvet bleu et on quitte enfin le
squat et son peuple nocturne. On arrive à Nation : qu'est-ce qu'on
fait ?

Deux groupes : Mélo, Christophe, Chichi, Momo, César, Anne, David et
Manu, je crois que c'est tout, vont terminer la soirée chez Jeff, qui
n'est pas là, mais qui habite pas loin ; et puis Adrian, Léna et moi, on
va se trouver un bus pour rentrer direct.

A l'arrêt, il y avait déjà des gens, un groupe de jeunes, ils sont là
depuis une demi-heure. Ils se demandent s'il y a vraiment des bus.
Justement, y'en a un qui arrive, mais il ne s'arrête pas, il est plein à
craquer et continue sa route vers gare de Lyon en nous laissant
poireauter pour le prochain. Les jeunes, dégoûtés, s'en vont à pied,
mais nous on reste. J'essaye le stop, tendre mon pouce, mais c'est pas
très efficace. Adrian propose le taxi, alors on remonte vers Nation pour
en chercher un, mais ils sont tous en service. C'est vraiment n'importe
quoi, on est trempés, il fait froid, mais personnellement je m'en fous,
dormir dans une cage d'escalier ne me fait pas peur, c'est juste que
Delanoë n'a carrément pas assuré son service de bus et ça c'est rageant
vu la pub qu'on nous sert autour de la Nuit Blanche, si si toute la nuit
je vous assure, mon cul, ouais.

Finalement, un bus arrive, c'est bon c'est le bon on monte direction
gare de Lyon et puis là il en faudra un autre mais ça on verra après.
Dans le bus, y'avait pas grand monde. On se met debout, au milieu. Un
gars devant moi, une pure caillera qui fait le malin devant ses potes au
fond du bus, insulte une femme, mais vraiment méchant, genre t'es pas
belle regardez-moi ça t'es une pute et j'en passe tellement c'était
écœurant. On ne dit rien, personne ne dit rien. Il continue de déverser
sa logorrhée et puis il lui crache dessus. La tension monte d'un cran,
cet espèce de connard retourne rigoler avec ses potes. Il revient, alors
que le bus s'arrête, et il crache une nouvelle fois sur la femme. Là,
j'explose : je ne sais plus ce que j'ai dit mais je l'ai savamment
insulté et si Léna ne m'avait pas retenu je lui aurais écrasé mon poing
sur la gueule. Ses potes le retiennent et ils descendent là. Je continue
à m'engueuler avec un gars dans le bus qui m'explique qu'il ne faut pas
chercher la merde et qu'il faut savoir fermer sa gueule et il me balance
même un range ton militantisme, j'hallucine. J'ai les nerfs en boule,
personne n'a rien dit, ils m'ont laissé faire le boulot à moi, mes 53 kg
et mes jambes flageolantes. J'explique au gars qu'au contraire il faut
arrêter d'être soumis et savoir ouvrir sa gueule quand c'est nécessaire,
et que là c'était intolérable. Je ne pense pas l'avoir convaincu.

Gare de Lyon. Je commence à désespérer du genre humain. Il pleut
toujours, c'est noir de monde, tous attendent un bus mais ceux qui
passent ne s'arrêtent pas, c'est vraiment impressionnant tout ces gens,
là, sous la pluie. Je m'écarte, contre un mur, je broie du noir, putain
si je l'avais frappé ça aurait pu se terminer à l'hôpital, quel con,
c'est la première fois que je cède à une pulsion bestiale comme ça,
n'empêche, j'aurais vraiment voulu lui éclater la tête, pour lui
apprendre la modestie, mais je suis pas sûr que ça lui aurait appris
quoi que ce soit. Je sens encore le bras de Léna qui me retient et je
reste frustré. Je commence à être vraiment trempé et frigorifié. Je
rejoins Léna et Adrian qui essaient de comprendre où s'arrête le bus qui
va à gare du Nord. En théorie on se sépare là : Adrian va vers Tolbiac,
chez lui, et Léna dort chez Mélo, donc on fait le trajet ensemble
jusqu'à gare du Nord. Mais toujours pas ou peu de bus et pas de taxi
libre. Adrian propose qu'on aille passer la nuit chez lui, c'est pas
trop loin et la ligne 14, automatique, fonctionne encore. On rentre dans
le métro, on croise Geoffroy, ex-bassiste de Glen or Glenda, disparu du
groupe presque du jour au lendemain. On échange trois phrases, concert
le 14 au squat RDC. On repart dans le métro. Arrivé à l'arrêt
Bibliothèque François Mitterrand, on réussit à attraper un bus qui passe
à Tolbiac, on ne fera pas le trajet sous la pluie. Il est trois heure et
demie. Je suis trempé, vanné, énervé, mais au final je m'amuse bien.

Chez Adrian, je mets mes pompes et mes chaussettes à sécher, je crois
que je ne connais rien de plus terrible que d'avoir froid aux pieds. Si,
peut-être, les oreilles, je supporte pas d'avoir les oreilles gelées. On
boit un thé, putain ça fait du bien un peu de chaud. On se met à refaire
le monde en écoutant *Made in USA*, BO de Sonic Youth. Je n'ai même pas
besoin d'avancer mes billes comme quoi l'altercation de tout à l'heure
était un problème de société et qu'il fallait que ça change, révolution.
Mais je me suis quand même expliqué sur pourquoi et comment et quel type
de société il nous faut générer, responsabilisation accrue de
tout-un-chacun, socialisme autogestion fédéralisme démocratie directe,
mais Adrian croit qu'il n'y a pas de révolution sans charnier et Léna ne
croit pas à la violence. On se dit quand même qu'on n'est pas passé loin
du pire, nous trois contre eux dix-douze, et Adrian ajoute heureusement
que les Argentins n'étaient pas là, parce que c'est vrai que César aviné
il peut avoir le direct facile. Léna se couche, avec Adrian on regarde
le début de *Don Giovanni* de Joseph Losey. Cinq heure et demie. On
arrête le film, Adrian rejoint sa piaule, je m'allonge sur le canapé. Je
n'arrive pas à dormir. Six heure et demie. La lueur du jour commence à
faire ressortir l'immeuble d'en face, isolé dans la grisaille. J'aurais
vraiment voulu le cogner, ce trou du cul, dans le bus, tout à l'heure.

La rancœur lycéenne

Il faudrait pouvoir vivre sa jeunesse
=====================================

Oublier les maîtres

L'enseignement de la servilité

L'avilissement dès 6 heures du mat

L'estomac acide et défoncé

Avec soi-même pour seul camarade

Les autres niais

Subir leurs compagnies

Discussions futiles avec des sans-âme

Une cour bétonnée où trône le platane hypocrite des mal à l'aise

Une architecture et des couleurs, si fausses !

Des cages auxquelles on n'échappe pas

(et votre dissertation ?)

NOOOON ! La cour remplie de leurs injonctions

Et mes pieds, oui, mes pieds, traversant cette cour

Des toilettes à la porte, de la porte aux toilettes

Vomir, encore, et chier sur leur gueule

L'autonomie ? une bonne blague, oui

Des rails, des grilles, des murs, gris

Des têtes, des visages, des ambiances, et toujours mal au ventre

IL N'Y A PERSONNE ICI ! QUE DES CADAVRES !

Pas de fuite, on la boucle et on déguste

Après tout, c'est l'école de la vie, c'était pareil avant et ce sera
pareil après

Se faire une raison ? On a le choix ?

(sortez !)

Mais où ?

L'ORDRE, apprendre à obéir

L'ORDRE, réponds quand on te parle

L'ORDRE, respecte ton supérieur plus que n'importe qui

Trois ans et votre ordre au panthéon des châtiments

(si ça ne vous intéresse pas, sortez !)

MAIS OU, CONNASSE ?

L'oppression ou le bâton, hein ? Et t'y prends ton pied ?

Quant à leur finalité, couverte de bons sentiments

Rien ne m'aura plus fait gerber

Il faut le savoir : la vie est ailleurs

Leur travail est un leurre

La réussite de misérables parasites égoïstes 

Mais non, mais non, ces années de clandestinité m'ont formé

Antisocial tous degrés

Hargne solide grâce à vous bien ancrée

N'ayez crainte, jamais je ne vous épargnerai

Considérations sur l'état du frigo
==================================

Il s'est mis en boule sur son lit, en chien de fusil. Pas question de
sortir maintenant. Il fait trop froid. Il n'ira pas travailler
aujourd'hui. Il est si bien, sous sa couette. Même s'il n'a plus
vraiment sommeil, parce qu'il a pris le rythme du travail, couché tôt,
levé tôt, il préfère rester sous sa couette. Pas forcément allongé.
Assis, le dos relevé par des coussins, position idéale pour lire ;
debout, levé, mais toujours sous sa couette, devant le frigo, pour
avaler un yaourt. Il sait pertinemment ce qu'il risque, s'il ne se lève
pas, mais faut pas pousser, ils se passeront bien de lui, et puis, il
faut bien l'avouer, il en a un petit peu rien à foutre. C'est même plus
mesquin que ça : son absence imprévue va les déstabiliser, ça va
bordéliser les cadences. Et ça, ça lui plait. Il imagine la tronche du
contremaître (oh le petit con s'il me bousille ma journée je vais être
furax et je n'aurais pas ma prime pourvu que le patron descende pas), du
patron qui évidemment se déplacerait sur les lieux pour pousser sa
gueulante (mais qu'est-ce que c'est que ce bordel), de la secrétaire
affolée comme si le ciel lui tombait sur la tête (mais pourquoi moi mon
dieu qu'est-ce que j'ai fait pour mériter ça c'est toujours sur moi que
ça tombe de bosser avec des fumistes mais peut-être qu'il a eu un
accident oui c'est ça je vais aller le dire au patron ah heureusement
que tu es là qu'est-ce qu'il ne feraient pas sans toi), des collègues
qui ne comprennent rien (il a du se bourrer la gueule après le match
d'hier c'est vrai qu'on s'est pris une sacrée branlée). Il en rigole
doucement, sous sa couette à carreaux. Il pense aussi à tout ce qu'il
pourra faire aujourd'hui, mais dehors il pleut, alors il va sans doute
rester là, télé (même s'il n'y a rien, c'est toujours pratique, ça vide
la tête), lessive, un bon gros chocolat chaud qui cale l'estomac pour la
journée, internet (même si ça coûte cher), téléphoner (pas des trucs
urgents, juste prendre des nouvelles des amis abandonnés) et lecture
(Les Inrocks et Libé achetés la veille, voire, s'il le sent, commencer
un des Jean-Claude Izzo parce que des amis marseillais lui rabâchent que
c'est géniaaaal). Il regarde son réveil : il est censé travailler dans
15 minutes. S'il se dépêche, il n'aura qu'une demi-heure de retard. S'il
part maintenant. Non. Il résiste encore. Bordel, sa couette est trop
attirante. Cette chaleur douillette et moite, il la quitterait pour le
fracas des machines ? Oh que non.

Mais il n'est pas tout seul. Il y a Ernest, le poisson rouge, à qui il
faut changer l'eau (ça fait un mois qu'il a la même) et à qui il faut
donner à manger. Ernest n'est pas très affectueux, et pour tout dire,
notre héros s'en balance un peu ; seulement voilà, il a promis à son
amie Framboise de s'en occuper pendant les trois mois qu'elle passait en
Thaïlande pour le compte de son boulot car elle doit y rencontrer des
éventuels investisseurs japonais et canadiens (oui, c'est curieux de les
rencontrer en Thaïlande, mais c'est parce que là bas c'est moins cher et
puis ça permet de voir du pays et d'allier travail et vacances,
exotisme, loisir, comme ça tout le monde y trouve son compte, hein) et
de toutes façons c'est toujours mieux de récupérer un poisson que des
plantes vertes (que Framboise a refilé à je ne sais pas trop qui) parce
que ça nécessite un peu moins d'attention (en un mot : c'est moins
chiant). Il va falloir qu'il se lève pour un poisson rouge. Situation
absurde, mais point rebutante. Il a connu des réveils plus humiliants,
ceux des jours où il allait bosser, par exemple.

Il regarde son réveil : 8 heures. Ca y est, on l'attend, on le cherche,
on se demande où il est. On s'avoue qu'on le trouve bizarre depuis
quelques temps ou carrément qu'on ne l'a jamais vraiment senti pour ce
genre de travail, mais on ne s'inquiète pas, on rigole, on se moque,
c'est gentiment antipathique, on cache qu'on ne l'aime pas sous les
dehors d'une bonne blague. Toutes ces pensées achèvent de le convaincre
de ne pas aller travailler aujourd'hui.

Il se lève pour Ernest, mais il se recouchera juste après. Il sort du
lit, entre en contact avec les élément glacés de l'atmosphère ambiante,
enfile un ticheurte estampillé 16^e^ conférence interrégionale des
producteurs de lait de brebis et un djinne pas trop sale et file dans la
cuisine en évitant de croiser le regard d'Ernest (lequel pourrait en
effet s'imaginer imminente la venue de sa pitance, mais il attendra,
comme tout le monde) pour boire ce merveilleux nectar multifruits vendu
pour pas cher en plus chez l'épicier arabe du coin. Il ouvre le frigo,
saisit la bouteille et boit de larges gorgées, en maintenant la porte du
frigo ouverte (il fait vraiment trop froid pour travailler). Il repense,
nostalgique, à sa couette, et repose la bouteille, ferme le frigo. Il
repasse rapidement devant Ernest, toujours sans s'arrêter, mais Ernest
n'a pas l'air très vivace ce matin, il allume la radio, France Info,
pour avoir les nouvelles fraîches, savoir si le monde tourne encore
rond, de ce rond si imparfait décidé par on ne sait trop qui mais imposé
à tous. Le monde tourne toujours rond, le voilà rassuré. Enfin, c'est
juste qu'il va pouvoir continuer à gérer son confort petit-bourgeois ;
dans l'absolu, il aimerait bien que ça change, il n'aurait plus à
travailler, mais voilà, quand on a la flemme, hé ben on s'habitue à se
vautrer sur un fauteuil en écoutant Bach, les doigts de pieds en
éventail.

Il repense à ses collègues : vu l'heure, ça doit être la panique à bord,
ils doivent être en train de réorganiser le plan de travail. Hé hé. Il
sourit.

Maintenant, il va s'occuper d'Ernest - à nous deux mon cochon -, il
remonte ses manches, se retourne vers l'aquarium, et il s'aperçoit avec
effroi qu'Ernest flotte, sur le dos, le ventre gonflé, sans bouger. Il
s'écrie merde Ernest joue pas au con t'es pas crevé quand même tu me
ferai pas ça pas toi oh merde je vais lui dire quoi à Framboise elle va
me tuer elle y tenait à son poisson t'es qu'un pauvre imbécile. Il reste
coi, les bras ballants, debout devant l'aquarium. Le désespoir le
saisit.

Sur ce, Ernest, content de sa blague, se retourne et se met à nager
normalement.

-   Non mais Ernest, tu te fous de ma gueule ?
-   Un petit peu, oui...
-   C'est malin, j'ai failli mourir de peur, tu sais bien que j'aime pas
    ça.
-   C'est ça qui est amusant, t'aurais vu ta tronche déconfite... Quel
    poilade, mon vieux !
-   Bon, c'est de bonne guerre, un point pour toi. Maintenant, si tu
    veux bien, je vais te changer l'eau, ça stagne un peu trop.

Il saisit un bocal vide, le plonge dans l'aquarium afin de capturer
Ernest, le ressort victorieux et s'en va vider l'aquarium dans l'évier.

-   Tu sais, Ernest, je ne suis pas allé travailler, ce matin.
-   Tu es malade ?
-   Non, j'avais pas envie, c'est tout.

L'aquarium de nouveau rempli, il jette Ernest dedans, en évitant toute
intrusion de l'eau souillée dans l'eau propre.

-   Et tu vas faire quoi de ta journée ?
-   Chais pas trop, je m'en fous un peu, je vais retourner au lit.

Le téléphone sonne. Il regarde l'heure : 8 heures 22. Il se demande qui
peut avoir l'idée saugrenue de l'appeler à une heure pareille. Le
téléphone continue de sonner. Doit-il décrocher ? Ca peut être
important. Il décroche.

-   Allo ?
-   Allo, monsieur XXX, c'est mademoiselle Yvette au téléphone (merde,
    la secrétaire).
-   Ah, bonjour (merde merde merde je sais pas quoi lui dire).
-   Alors qu'est ce qui vous arrive, pourquoi vous n'êtes pas venu (gna
    gna gna)?
-   Euh, oh, je suis malade, un peu, et je ne me suis pas réveillé
    (bonjour l'impro c'est minable t'aurais jamais du arrêter les cours
    de théâtre), mais j'allais vous appeler.
-   Oui oui, je n'en doute pas (menteuse), vous ne venez pas
    aujourd'hui, donc ?
-   C'est ça, je vais me reposer un peu.
-   La prochaine fois, vous me prévenez avant 8 heures, parce que là,
    quand même, ça perturbe un peu tout (oh ce délicieux ton de
    reproche, je vous emmerde, Yvette, je vous emmerde).
-   Oui oui. Au revoir.
-   Et vous m'amènerez le certificat du docteur (merde elle fait chier)
    pour que je remplisse les papiers.
-   C'est ça, à demain.
-   Si je vous embête, dites-le, hein, je ne fais que mon boulot (pauvre
    Yvette), et j'essaie de le faire bien, moi (ça sent la crise de
    larmes).
-   Mais non, vous êtes adorable, qui pourrait vous en vouloir ? Si je
    suis un peu sec avec vous c'est parce que je suis malade.
-   Ne vous foutez pas de ma gueule !

Elle lui raccroche au nez. Ah ben ça, il ne s'y attendait pas. Il repose
le combiné.

Il reste quelques secondes éberlué devant le téléphone : c'est
maintenant que ça devient intéressant, elle va se plaindre au patron et
il risque de se faire lourder (ça fait une remontrance de plus à son
égard). Il lui faut assumer les conséquences, s'il ne lui avait pas
montré la vraie nature de ses sentiments à son égard Yvette pardon
mademoiselle Yvette ne se serait pas fâchée. Mais il ne regrette rien,
il n'est pas du genre hypocrite du quotidien. Seulement, s'il est viré,
il lui faudra trouver un autre emploi, et ça, ça va pas être de la
tarte, il n'a envie de rien, surtout pas de chercher un travail, de se
vendre, d'exhiber son CV plus que modeste et d'aller à des rendez-vous
d'embauche rencontrer des gens dont il se moque éperdument.

Il se retourne vers Ernest, avec un sourire contrarié.

-   Merde, dit-il.
-   Allons, y'a pire, ta vie ne s'arrête pas là.
-   Déconne pas, si je me fais virer, j'ai plus de salaire, et je fais
    quoi, après ?
-   Alors juste avant de te faire virer, tu me donnes à manger, s'il te
    plait. Après, on a toute la journée pour éplucher tes possibilités
    de reconversion.

Il attrape la boîte ronde à côté de l'aquarium et la secoue au-dessus.

-   N'en met pas trop, je veux pas finir obèse.
-   Je me dis qu'il faudrait peut-être que j'y aille cet aprèm, je dirai
    que je n'ai pas vu de médecin parce que je me sens mieux et que je
    suis tout à fait apte à reprendre le travail.
-   Ah, non, arrête, tu peux pas faire ça, c'est absurde, tu va pas leur
    céder une fois de plus... T'avais prévu de retourner au lit,
    pourquoi t'es encore debout ?
-   En fait, j'ai plus vraiment sommeil, figure toi qu'un tel coup de
    fil ça secoue un peu. D'ailleurs, je vais manger.

Il se dirige vers la cuisine, sort du frigo saucisson et yaourt vanillé,
concombre entamé et raisins secs. Il s'assied et mange, perdu dans ses
pensées. C'est stupide, il n'ira pas travailler aujourd'hui. C'est
stupide, il risque de se faire virer. Tout est stupide, lui au premier
rang. Ce yaourt aussi : combien sont en train de trimer pour produire
des yaourts vanillés meilleurs que ceux du concurrent ? Société stupide.

Il pense à sa couette et se dit qu'il serait plus au chaud avec elle sur
les épaules, mais en même temps c'est un peu risqué, une tâche est vite
arrivée, et sa couette, il faut qu'elle dure. Il se rend compte alors
qu'il se sépare pour la journée de sa couette (le drame) et que s'il ne
voulait pas se lever c'était pour rester sous sa couette mais non il a
fallu qu'il se lève quand même et tant pis pour la couette maintenant
qu'il est levé et de surcroît stressé par le coup de fil de mademoiselle
Yvette il ne peut pas se recoucher car il sait qu'il ne pourra retrouver
cette insouciance caractéristique du dormeur bienheureux. Merde. La
journée s'annonce rude.

Il regarde l'heure : il n'est pas encore neuf heures. Son festin
terminé, il retourne voir Ernest.

-   Mon bon Ernest, je te préviens, pour toi la tâche s'annonce ardue :
    il va falloir que tu me change les idées.
-   Je serai à la hauteur.

Ernest est des fois un peu mégalo, il a tendance à trop se prendre au
sérieux. Il continue.

-   En fait je serai surtout apte à te raconter des trucs sur Framboise,
    je ne connais pas grand chose d'autre, vu qu'en général je ne sors
    pas de chez elle.

Il s'assied sur son fauteuil favori, se cale la tête avec un coussin, et
prend la parole.

-   Ne t'en va pas tout me raconter sur Framboise... Je suis sûr qu'elle
    te dit plein de choses mais je ne sais pas si ça lui ferait plaisir
    de savoir que tu m'as tout raconté...
-   Si tu préfères je peux te raconter le dernier truc que j'ai vu sur
    TF1.
-   Framboise regarde TF1 ?
-   Ben comme tout le monde, ça arrive, elle a laissé la télé allumé et
    elle est partie sous la douche.
-   Je préfèrerais que tu me raconte Framboise sous la douche.
-   Ah, malheureusement, quand je suis chez elle, je ne peux pas la voir
    dans la salle de bains, je n'ai pas cet angle de vue.
-   Ah oui, c'est vrai.
-   Par contre, je peux te raconter Framboise nue ; elle ne se
    déshabille pas que dans la salle de bain.
-   Oh la la, le vieux fantasme... Vas-y, je t'écoute... Mais juste une
    question, d'abord : elle est célibataire ?
-   Ces temps-ci, oui. Elle a quitté son machin, là, le Jean-Paul
    Ducoin, j'ai jamais pu le sentir, celui-là. Chaque fois qu'il me
    regardait j'avais l'impression qu'il voulait me gober tout cru. Tu
    l'as connu ?
-   Non, mais tu sais, moi, Framboise, je la vois pas si souvent que ça,
    en fin de compte...
-   Comment tu l'as rencontrée ?
-   Ooh, c'est... Ecoute, j'ai pas très envie de me replonger là dedans,
    cette période là, j'assume pas encore très bien, je préfèrerais ne
    pas en parler... C'est un peu confus...
-   Très bien, très bien. Mais... Excuse-moi mais...
-   Vas-y, je t'en prie.
-   T'étais amoureux ?

Il se passe 5 secondes de silence. Il tourne la tête, fait des gestes
inutiles et inaboutis avec les bras (comme une démonstration que l'on
sait par avance foireuse), souffle un peu, et finit par répondre.

-   Oui. Au début, oui.

Silence. Pas un silence pesant, plutôt un silence qui précède un tour de
force. Il continue.

-   Mais ça n'a jamais abouti. Je ne sais pas trop, mais je crois qu'en
    définitive elle n'était pas amoureuse de moi. Je ne lui ai jamais
    demandé, je ne lui ai jamais dit ce que je ressentais pour elle.
-   Pourquoi ?
-   Rapidement, je suis devenu trop proche d'elle, à recueillir des
    confidences, et... Ben c'est comme si j'avais les mains liées,
    j'étais confident, et pas prétendant.

Courte pause. Il continue.

-   Je voulais pas en parler, tu m'as un peu forcé la main, hein,
    salopard, va.
-   Allez, c'est bon, lâche-toi, ça ne peut que te faire du bien.

Il se redresse un peu, appuie se coudes sur ses genoux et se gratte le
crâne. Une idée lui traverse l'esprit : et s'il quittait la pièce, s'il
fuyait cette conversation embarrassante ? Ah ça oui, il saurait faire.
Cette lâcheté là, il maîtrise. Mais non. Il sait qu'il faut qu'il reste.
Il décide de rester. Mais de se taire. Non, c'est absurde, autant
partir. Finalement, il se lève et met un disque dans sa chaîne. C'est *A
Tribute To Jack Johnson* de Miles Davis. Là, il se sent un peu plus à
l'aise. Il se rassoit. La guitare l'excite, et la rythmique l'emballe.
Rien à redire, c'est de la bonne. Ernest reprend.

-   Je comprend ce que tu ressent. Frustré.
-   Oui, quelque part oui. Mais plus maintenant, enfin plus vraiment,
    c'est du passé, tout ça.
-   Tu veux toujours que je te raconte Framboise nue ?
-   Ah ouais, vas-y, ça n'empêche rien, je crois que je la connais trop
    pour désirer avoir une relation avec elle mais tu m'as mis l'eau à
    la bouche.
-   J'ai juste peur de la démystifier... Tu sais, elle est vraiment
    belle quand elle est nue... Des fois, quand elle sait qu'elle n'a
    rien à faire de la journée et qu'il fait chaud, elle ne s'habille
    pas et reste comme ça toute la journée. Elle est fine, élancée, peau
    claire, attirante, quoi, enfin je le conçois volontiers...
-   Je crois qu'elle doit ressembler à ce que j'imagine, à ce que
    j'aimerai qu'elle soit.

Il sourit, benoîtement, engoncé dans son fauteuil. Le temps s'arrête, il
ne pense à rien d'autre qu'à Framboise. Mais petit à petit le quotidien
reprend son oppression latente. Il se remémore la fâcheuse conversation
téléphonique avec mademoiselle Yvette et considère que si c'était à
refaire il agirait de même ; cependant, il déplore les éventuelles
conséquences dramatiques d'un tel acte (putain si je suis sur le carreau
c'est la merde), sans pour autant se remettre en question. Il oscille
quelques instants entre Framboise et mademoiselle Yvette. L'image de
l'une succède à l'image de l'autre, ce n'est pas un choix, ce n'est pas
lui qui décide, c'est un duel très manichéen, le fantasme contre la
réalité, il a peur de se laisser gagner par le stress qui augmente, il
se dit que de toute façon le fantasme est condamné à péricliter alors il
préfère abdiquer et replonger dans la réalité. Il se lève.

-   Bon, c'est pas tout, ça, j'ai quand même des trucs à faire.

Phrase lancée machinalement, d'autant plus stupide qu'il n'a rien à
faire. Il retourne dans la chambre, se prépare à prendre une douche tout
en pensant que finalement cette journée s'annonce bien compliquée et
qu'il aurait peut-être mieux fait d'aller travailler. Non. Il ne peut se
résoudre à considérer son refus d'aller travailler comme quelque chose
d'inconsidéré, d'immature ; il sait qu'il est dans son bon droit. Il
sait qu'il a eu raison de ne pas aller travailler. Sous la douche (merde
c'est encore bouché) il repense à la conversation qu'il a eue avec
Ernest et à ce qu'il lui a arraché, à ce qu'il a avoué, et se dit que ce
n'est peut-être pas la peine de refouler cette histoire comme ça, de
toute façon ça sortira tôt ou tard et forcément Framboise sera au
courant un jour. Comment le prendra-t-elle ? Tendrement (elle se jette
dans ses bras) ou ironiquement (elle rigole et lui dit qu'elle l'aime
bien quand même), elle est capable de tout, Framboise. Il sort de la
douche, l'image de Framboise nue lui traverse l'esprit et il a un début
d'érection (ah non pas maintenant). Il enfile les même fringues que la
veille (plus grand chose de propre) et se prépare à sortir, bien que ce
soit contraire à son plan prévisionnel. Du pain. Hé oui, il lui faut
acheter du pain. Mais dehors, il fait froid et il pleut, enfin la pluie
vient de cesser, alors il enfile un pull en laine, une bonne surchemise,
lace ses baskets un peu moisies (faudra en racheter d'autres), et se
revêt d'une parka noire, résidu d'un vieux stock de l'armée, achetée pas
très cher dans une friperie. Il attrape ses clés, jetées nonchalamment
la veille sur une chaise destinée à voir s'accumuler tout ce que peuvent
contenir ses poches, cherche dans ce bordel son porte-monnaie,
l'enfourne, et franchit la porte sans grande joie. Sortir ne
l'enthousiasme guère. Le contact des gens ne lui est pas désagréable,
non, mais il n'aime pas être vu, observé, critiqué, catalogué. Le monde,
c'est lui qui l'aborde, pas l'inverse. Et le monde, il n'a qu'a bien se
tenir, parce qu'aujourd'hui, mine de rien, il commence à être
passablement énervé.

De retour chez lui, sa baguette à la main, il va d'abord vérifier son
répondeur avant de se dévêtir ; on ne sait jamais, on n'est jamais à
l'abri d'un mauvais coup du sort, peut-être un sinistre message
l'attend, la journée a mal commencé, il ne voit pas de raisons pour
qu'elle continue autrement. Bingo : un nouveau message. Mais il ne
l'écoute pas de suite, il hésite. Il serait plus souhaitable qu'il se
déshabille d'abord, mais il a la flemme, cette fatigante flemme qui lui
donne envie de retourner sous sa couette. Du coup, à force
d'hésitations, il ne fait rien, il ne bouge pas, il attend que la
décision vienne (allez, réagis, mais tout ça c'est parce que t'es
crevé), puis il se dit qu'il a le temps. Il enlève donc d'abord son
manteau, passe devant Ernest et lui lance un regard désabusé (ben oui
mon vieux c'est pas tout rose), et pose sa baguette dans la cuisine.
Mais là, il bloque, encore : l'envie d'aller écouter ce message est
contrecarrée par la quasi certitude de n'y trouver que mauvaises
nouvelles et autres ennuis en perspectives. Si ce n'est pas mlle Yvette
ou son patron, c'est sans doute cet imbécile de standardiste de la CAF
qui l'harcèle depuis trois semaines pour une prétendue somme versée
accidentellement et que de toute façon il ne rendra pas (ils ne vont pas
pleurer pour cinq cent balles), ou alors c'est Etienne qui décommande le
week-end en Ardèche ou Marie qui\... Marie qui quoi ? Marie qui rien,
faut pas rêver, elle ne l'appellera jamais. C'est peut-être sa mère, pas
besoin d'expliquer pourquoi ça l'énerve. Il stresse, en sachant que
c'est stupide, mais il n'y peut rien. Il va d'abord allumer son
ordinateur, ça détournera l'attention et peut-être ça estompera son mal
de ventre naissant. Ce n'est certes qu'un message sur un répondeur, mais
c'est quand même angoissant. Il se connecte à Internet, vérifie ses
boîte à mails (ça sert vraiment à rien d'en avoir trois) mais personne
de bien important ne lui écrit. Il se ballade un peu, visite des sites
déjà connus, puis se rend compte qu'il doit être difficilement joignable
par téléphone vu qu'il occupe la ligne et que ça peut poser un problème
si on cherche à le joindre, comme c'est manifestement le cas, à en
croire le message qui attend sur le répondeur. Une idée lui traverse
l'esprit : puisqu'il appréhende tant ce message, tiens, il va prendre
les devants et appeler tous les gens susceptibles de l'avoir appelé lui.
Sans écouter le message. Il se tourne vers Ernest.

-   Dis-moi, Ernest, qui est-ce qui pourrait avoir l'idée saugrenue de
    me contacter aujourd'hui ?
-   Je sais pas, moi, je suis pas devin, je suis juste poisson rouge.
-   Certes, mais un poisson rouge de bon conseil. En fait, je vais
    appeler Etienne, Marie et Amaury, pourquoi pas. Les autres, hein,
    ils m'appelleront, c'est pas à moi à faire les démarches. Si c'est
    le travail, ils iront se faire foutre. Si c'est ma mère, tant pis,
    elle rappellera.

Il se rapproche du téléphone et compose le numéro d'Amaury, et tombe sur
son répondeur (c'est bien la peine d'avoir un portable). Légèrement
désemparé, il lui laisse un message où il bafouille en expliquant qu'il
voulait avoir de ses nouvelles et bla bla bla les formules d'usage.
C'est un chouette gars, Amaury, il voudrait le connaître un peu plus.
Dans la foulée il appelle Etienne, et il tombe encore sur un répondeur,
mais cette fois-ci le message qu'il laisse est plus clair (on se la fait
ou quoi cette virée). Et bien sûr, quand il veut appeler Marie, ça
bloque. En fait, il aimerait bien tomber sur son répondeur. Aussi il
prépare son texte à l'avance, comme ça il n'aura plus qu'a le réciter.
Mais forcément, il le sent, ça ne va pas se passer comme ça. Marie, elle
lui plaît, un petit peu, il ne la connaît pas des masses non plus, mais
il aime bien penser à elle, quand elle est là ça le rend tout chose (ses
yeux brûlent). Il se lance et tape son numéro. A la troisième sonnerie,
elle décroche.

-   Allo...
-   Euh oui salut c'est moi (merde merde merde je lui dis quoi là ?).
-   Tu vas bien ?
-   Oh oui, tranquille, comme d'hab. Et... en fait j'appelai juste pour
    avoir de tes nouvelles, quoi.
-   Hé bien écoute, moi, ça peut aller, mais aujourd'hui c'est
    particulier, je me repose, je ne fais rien.
-   Aaah... Figures-toi que moi, pareil. Aujourd'hui, je glande. Et...
    voilà, quoi (putain embraye trouve un truc à dire).
-   Enfin moi c'est particulier j'étais censée aller travailler mais je
    ne me suis pas réveillée à temps, alors je n'y suis pas allée, tu
    vois, j'ai eu la flemme.
-   Mais c'est rigolo, c'est vraiment pareil pour moi, juste que moi
    j'étais réveillé mais c'est surtout que je n'ai pas voulu y aller,
    alors je suis resté chez moi.
-   Ah ouais...
-   Ben c'est curieux (t'es nul c'est foutu tu vas passer pour un
    demeuré).
-   Oui, curieux, c'est le terme.
-   Euh juste avant de raccrocher je voulais savoir si justement c'était
    possible qu'on se revoit un de ces quatre, pour aller boire un verre
    (tu l'as fait tu l'as fait).
-   Oui, sans problème, tu m'appelle si vous faîtes une descente en
    ville, ou...
-   En fait j'aurais aimé te voir mais sans les autres.
-   Oui. (merde un blanc c'est pas bon dis quelque chose).
-   Et donc ce soir par exemple, tu fais quoi ?
-   Rien, mais je ne suis pas libre avant dix heures...
-   C'est trop tard pour moi pour ce soir mais c'est pas grave. Je te
    rappellerai. Demain ?
-   OK, si tu veux.
-   Bon faut que j'y aille (amertume). Tchao et à bientôt.
-   Tchao.
-   Bye.

Il raccroche. C'est raté pour aujourd'hui. La déprime guette. Ca a foiré
complet. Putain quand tout s'y met, c'est le bouquet : le boulot qui
fait chier et l'histoire avec Charlie qui ne décolle pas d'un
centimètre. Dans sa tête, mademoiselle Yvette, Framboise, Marie, toutes
se mélangent et ne forment plus qu'une seule personne, genre de cadavre
exquis plus hydre que bombe sexuelle (t'es qu'un lâche un minable). Dans
la série des mauvaises nouvelles, il se rappelle qu'il n'a toujours pas
écouté le message sur le répondeur, et il est de plus en plus persuadé
qu'il ne peut s'agir que d'une mauvaise nouvelle. Il se lève et se
vautre sur son canapé. Il feuillette les Inrockuptibles, rois de la
gauche culturelle molle, y lit quelques brèves néanmoins loin d'être
inintéressantes, se demande où sont passés les militants, les vrais, les
radicaux, il en a bien existé, et où sont-ils, maintenant, hein, que
font-ils, ils dorment ? ah non, ils militent ? ils sont bien discrets,
ou alors affaiblis, ou alors pas très radicaux, va savoir, ce serait
plus très étonnant. Il repense à Marco. Marco, c'est un militant. Paraît
qu'il ne va pas mieux. Il se demande ce qui peut bien clocher chez
Marco, il a cette prestance du gars droit, sûr de lui et qui n'hésite
pas. Il se remémore leur dernière véritable conversation (jour de pluie
et café bondé :

-   C'est depuis que Carlo est mort.

Trop de fumée dans ce café.

-   Je ne les supporte plus. Je ne les aimais déjà pas beaucoup avant.

Il ne dit rien, il l'écoute.

-   Quand j'en croise dans la rue, ça me rend nerveux.

Il ne bouge pas, stoïque, il est marqué, ça se sent.

-   En plus j'y étais pas, quand il l'ont tué, mais tu vois, c'est...

Il ne termine pas sa phrase.

-   Je sais pas, je peux pas trop matérialiser ça.

Un ange passe.

-   Je crois que j'ai peur des flics, maintenant.

Ah ça, c'est ennuyeux.

-   Je peux plus militer. Je sens qu'il faut que je raccroche.

Il baisse les yeux, se concentre sur son verre.

-   Je vais finir par péter les plombs, sinon.

Il joue avec son briquet.) Il se rappelle brutalement ce message sur son
répondeur. Ca y est, là, il est stressé, y'a rien à faire. Y'a qu'a
l'écouter, ce message, ne pas faire de chichis pour si peu, y'a pire
dans une vie, merde, écouter un message au répondeur, c'est absurde, il
ne va pas flancher là, il se lève, il s'approche du répondeur. Et puis
rien. Il bloque. Rien. Il se tourne vers Ernest.

-   T'y arrives pas, hein ?
-   Ben non, tu vois bien, c'est la première fois que ça ma fait ça.
-   T'as peur de quoi, au juste ?
-   Je sais pas si j'ai peur, mais je sens que ça va être mauvais. Le
    boulot, je les emmerde, l'Ardèche, ça me ferait chier de pas pouvoir
    y aller...
-   C'est peut-être une confirmation.
-   Ouais. J'y crois pas.
-   Pourquoi ?
-   Je sais pas. Mais peut-être que j'ai pas envie de l'écouter, tout
    simplement. Comme si j'étais injoignable, ou disparu.
-   C'est peut-être Framboise qui veut prendre de mes nouvelles.
-   Mouais. C'est moins probable. Sans vouloir te vexer.
-   Trop tard.
-   Ernest, sois mignon, arrête tes simagrées.
-   T'es pète-couilles, hein, allume ta télé, avachi sur ton canapé, et
    tu penseras à autre chose qu'à ton message.
-   Ah, bonne idée, merci, j'y avais pas pensé.
-   Gna gna gna.

Perturbé, il va dans sa chambre, se demande ce qu'il est venu y faire,
reviens sur ses pas, entre dans la cuisine, se sert à boire tout en se
disant qu'il va bientôt devoir manger (il est déjà midi ?), boit,
ressort, espère sortir et voir du monde parce que sinon il va péter un
plomb tout seul ici à tourner en rond, il s'était dit en se réveillant
qu'il resterait sous sa couette, et voilà où il en est, il retourne dans
sa chambre et se dit MERDE y'a quelque chose qui cloche je fais quoi là
je suis qui c'est quoi cette crise il contemple sa couette stoïque il
ressort de la pièce se prend la tête entre les mains allume la télé
coupe le son va mettre un disque c'est *Trompe Le* *Monde* ça au moins
c'est bon il s'assoit face à la télé putain de répondeur de merde putain
de travail à la con mais c'est pas vrai les gens sont cons pourquoi je
suis bloqué MEEEEEEEEERDE elle me plaît faut que je fonce mais il ne
fait rien ça coince et il se laisse absorber par le flux sans fin et
sans fond des images connes et il reconstitue leur vie et se dit qu'il
ferait mieux de.

sommaire

\- page 1 : *de la politique comme ressentiment inné de tout être et de
son application concrète et imminente sur terre et pourquoi pas aussi
dans les limbes de l'hyperespace*

\- page 20 :* nuit blanche*

\- page 25 : *la rancœur lycéenne*

- page 27 : *considérations sur l'état du frigo*
------------------------------------------------

tous textes © bastien roche 2003
--------------------------------

l'édition originale de *«De la politique comme ressentiment inné de tout
être et de son application concrète et imminente sur terre et pourquoi
pas aussi dans les limbes de l'hyperespace » et autres nouvelles* a été
tirée à 4 exemplaires

achevé d'imprimer à montpellier le 20 vendémiaire 212 
------------------------------------------------------

(14 octobre 2003)
-----------------

mis en page par usul tomchin
