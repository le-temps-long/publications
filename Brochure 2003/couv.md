bastien roche

de la politique comme ressentiment inné de tout être et de son
application concrète et imminente sur terre et pourquoi pas aussi dans
les limbes de l'hyperespace

![](Pictures/100000000000040B0000031BC34E17A23A2B6908.jpg){width="12.726cm"
height="9.818cm"}

et autres nouvelles

« *J'étais au café, à la terrasse, avec Sophie. Ricou, qui milite avec
moi au PD, a déboulé comme un renard pris en chasse et s'est assis sans
demander s'il dérangeait.*

-   Trotspin...
-   Oui ? Hé ben, il a quoi, Trotspin ?
-   Il est joskiste !

*La révélation nous fit l'effet d'une bombe, le bruit, les débris et les
bouts de viande en moins. Il n'y a qu'en Palestine que les bombes
explosent aux terrasses des cafés. Pas chez nous, tout de même. On sait
vivre, en France.* »
